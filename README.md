# Uptime Monitor

Simple uptime monitor making use of [Patrol](https://github.com/karimsa/patrol)

## Getting started

Ensure that you have the following installed:

- Docker
- Docker Compose plugin

This should work on all OS variants.


```bash
git pull https://gitlab.com/jc1arke/uptime-monitor.git
cd uptime-monitor
docker compose up -d
```

To view logs:

```bash
docker compose logs --tail=10 -f
```

To amend the destinations, see [./config/patrol.yml](./config/patrol.yml) and make adjustments as needed.

The application will be available on [http://localhost:8080/](http://localhost:8080/) unless you make use of Traefik or the like in front of it.